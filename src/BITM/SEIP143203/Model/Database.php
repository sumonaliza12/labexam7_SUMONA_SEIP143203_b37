<?php


namespace App\Model;

use PDO;
use PDOException;


class Database
{

    public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_b37";
    public $username="root";
    public $password="";

    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->DBH = new PDO("mysql:host=localhost;dbname=$this->dbname", $this->username, $this->password);
          echo "Database successfully connected!!";

        }
        catch(PDOException $msg) {
            echo $msg->getMessage();
            echo "failed";
        }

}

}
$obj = new Database();