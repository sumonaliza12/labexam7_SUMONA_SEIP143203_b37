<?php
namespace App\SummaryOfOrganization;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class SummaryOfOrganization extends DB{
    public $id="";
    public $name="";
    public $details="";

    public function __construct(){
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name = $data['name'];
        }
        if(array_key_exists('details',$data))
        {
            $this->details = $data['details'];
        }
    }
    public function  store()
    {

        $query = $this->DBH-> prepare("INSERT INTO summary_of_organization(name,id,details)
        VALUES(:name,:id,:details)");

        $query->execute(array(
            "name" => $this->name,
            "details" => $this->details,
            "id"=>$this->id

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ Name: $this->name ] , [ Details: $this->details ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ Name: $this->name ] , [ Details: $this->details ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}