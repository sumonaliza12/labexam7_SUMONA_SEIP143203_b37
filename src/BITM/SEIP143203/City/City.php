<?php
namespace App\City;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class City extends DB{

    public $name="";
    public $city="";

    public function __construct(){
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {

        if(array_key_exists('name',$data))
        {
            $this->name = $data['name'];
        }
        if(array_key_exists('city',$data))
        {
            $this->city = $data['city'];
        }
    }
    public function  store()
    {

        $query = $this->DBH-> prepare("INSERT INTO city(name, city)
        VALUES(:name,:city)");
        $query->execute(array(
            "name" => $this->name,
            "city" => $this->city,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ Name: $this->name ] , [ City: $this->city ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ Name: $this->name ] , [ City: $this->city ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}